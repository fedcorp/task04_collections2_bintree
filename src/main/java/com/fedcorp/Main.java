package com.fedcorp;

import java.util.Map;
import java.util.TreeMap;

public class Main {
    public static void main(String[] args) {

        MyBinTree<String, Integer> tree = new MyBinTree();
        System.out.println(tree.size());
        tree.put("1",100);
        tree.put("2",200);
        tree.put("3",300);
        tree.put("4",400);
        tree.put("5",500);

        System.out.println(tree.size());

        System.out.println(tree.get("5"));
        System.out.println(tree.get("4"));

        tree.remove("1");

        System.out.println(tree.size());
        System.out.println(tree.get("4"));
    }
}
