package com.fedcorp;

public class MyBinTree<K extends Comparable, V> {

    private class Node<K, V> {
        K key;
        V value;
        Node leftChild;
        Node rightChild;
    }

    private Node root;
    private int size;

    MyBinTree() {
        root = null;
        size = 0;
    }


    public int size() {
        return this.size;
    }

    public Object get(K key) {
        Node current = root; // Начать с корневого узла
        while (current.key != key) // Пока не найдено совпадение
        {
            if (key.compareTo(current.key) < 0) // Двигаться налево?
                current = current.leftChild;
            else // Или направо?
                current = current.rightChild;
            if (current == null) // Если потомка нет,
                return null; // поиск завершился неудачей
        }
        return current.value; // Элемент найден
    }

    public Object put(K key, V value) {
        Node newNode = new Node();
        newNode.key = key;
        newNode.value = value;
        if (root == null) {
            root = newNode;
            size++;
            return null;
        } else {
            Node current = root;
            Node parent;
            while (true) {
                parent = current;
                if (key.compareTo(current.key) == 0) {
                    newNode.value = current.value;
                    current.value = value;
                    return newNode.value;
                }
                if (key.compareTo(current.key) < 0) {
                    current = current.leftChild;
                    if (current == null) {
                        parent.leftChild = newNode;
                        size++;
                        return null;
                    }
                } else {
                    current = current.rightChild;
                    if (current == null) {
                        parent.rightChild = newNode;
                        size++;
                        return null;
                    }
                }
            }
        }
    }

    public Object remove(K key) {
        if (root != null) {
            Object delVal;
            Node current = root;
            Node parent = root;
            boolean isLeftChild = true;
            while (current.key != key) {
                parent = current;
                if (key.compareTo(current.key) < 0) {
                    isLeftChild = true;
                    current = current.leftChild;
                } else {
                    isLeftChild = false;
                    current = current.rightChild;
                }
                if (current == null) return null;
            }
            delVal = current.value;
            if (current.leftChild == null &&
                    current.rightChild == null) {

                if (current == root) {
                    root = null;
                } else if (isLeftChild) {
                    parent.leftChild = null;
                } else {
                    parent.rightChild = null;
                }

            } else if (current.rightChild == null) {
                if (current == root) {
                    root = current.leftChild;
                } else if (isLeftChild) {
                    parent.leftChild = current.leftChild;
                } else {
                    parent.rightChild = current.leftChild;
                }
            } else if (current.leftChild == null) {
                if (current == root) {
                    root = current.rightChild;
                } else if (isLeftChild) {
                    parent.leftChild = current.rightChild;
                } else {
                    parent.rightChild = current.rightChild;
                }
            } else {

                Node successor = getSuccessor(current);

                if (current == root) {
                    root = successor;
                } else if (isLeftChild) {
                    parent.leftChild = successor;
                } else {
                    parent.rightChild = successor;
                }
                successor.leftChild = current.leftChild;
            }
            size--;
            return delVal;
        }
        return null;
    }

    private Node getSuccessor(Node delNode) {
        Node successorParent = delNode;
        Node successor = delNode;
        Node current = delNode.rightChild;
        while (current != null) {
            successorParent = successor;
            successor = current;
            current = current.leftChild;
        }

        if (successor != delNode.rightChild) {
            successorParent.leftChild = successor.rightChild;
            successor.rightChild = delNode.rightChild;
        }
        return successor;
    }


}



